#!/usr/bin/env bash

SHELL=/bin/bash

TIMESTAMP=`date +%Y%m%d%H%M%S`


DOCKER_BASE=/swissbib/harvesting/docker.consumercbs
CONFDIR=$DOCKER_BASE/configs

cd $DOCKER_BASE

DEFAULTREPOS="abn alex alexrepo bgr boris ecod edoc eperiodica ethresearch hemu idsbb idslu idssg kbtg libib nb posters sbt serval sgbn vaud zora nebis rero"


if [ -n "$1" ]; then
  repos=$*
  repo_force=1
else
  repos=${DEFAULTREPOS}
  repo_force=0
fi

for repo in ${repos}; do
    LOCKFILE=${CONFDIR}/${repo}.lock

    if [ -e ${LOCKFILE} ]; then
        echo -n "${repo} is locked, probably by another cbsconsumer container: "
        cat ${LOCKFILE}
        continue
    else
        echo $$ >${LOCKFILE}
    fi


    echo "start container for $repo at : ${TIMESTAMP}\n" >> ${DOCKER_BASE}/container.log
    docker container run --rm -v $DOCKER_BASE/logging:/logging -v $DOCKER_BASE/configs:/configs -v $DOCKER_BASE/results:/results -v $DOCKER_BASE/archive:/archive -v $DOCKER_BASE/backpressure:/backpressure cbsconsumer java -Dlog4j.configurationFile=/logging/cbsconsumer.yaml -jar /app/consumercbs-0.2-all.jar -s cbs${repo}.yaml -c /configs

    TIMESTAMP=`date +%Y%m%d%H%M%S`
    echo "finished container for $repo at : ${TIMESTAMP}\n" >> ${DOCKER_BASE}/container.log

    rm ${LOCKFILE}

done



