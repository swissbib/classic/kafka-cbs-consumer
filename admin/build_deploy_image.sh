#!/usr/bin/env bash


CONSUME_CBS_BASE=/home/swissbib/environment/code/swissbib.repositories/kafkajava/consumercbs
TARGET=/swissbib/harvesting/docker.consumercbs

cd $CONSUME_CBS_BASE

echo "build new latest image kafka-event-hub"
docker image build --no-cache -t cbsconsumer .

echo "save latest image cbsconsumer as tar file"
docker save cbsconsumer --output cbsconsumer.tar

echo "cp tar file to target host"
scp cbsconsumer.tar harvester@sb-ucoai2.swissbib.unibas.ch:$TARGET

echo "rm already existing image om target host"
echo "load just created image on target host"
ssh harvester@sb-ucoai2.swissbib.unibas.ch "cd $TARGET; docker image rm cbsconsumer; docker load --input cbsconsumer.tar"



echo "cp config and admin scripts on target host"
scp -r admin configs harvester@sb-ucoai2.swissbib.unibas.ch:$TARGET
scp  logging/cbsconsumer.yaml harvester@sb-ucoai2.swissbib.unibas.ch:$TARGET/logging


rm cbsconsumer.tar

ssh harvester@sb-ucoai2.swissbib.unibas.ch "rm $TARGET/cbsconsumer.tar"
