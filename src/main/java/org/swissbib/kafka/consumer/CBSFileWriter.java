package org.swissbib.kafka.consumer;


import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.swissbib.kafka.consumer.config.ConfigHandler;
import org.swissbib.kafka.consumer.exceptions.FileWriterException;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.concurrent.ThreadLocalRandom;
import java.util.zip.GZIPOutputStream;

import static java.time.format.DateTimeFormatter.*;





public class CBSFileWriter {

    //todo: create a lambda to minimize write code
    @FunctionalInterface
    interface Writer {

        void write (OutputStream stream, String content) throws IOException;
    }

    private static final Logger fileWriterLogger = LogManager.getLogger(CBSFileWriter.class);



    private ConfigHandler config;
    private String archiveDir;
    private String outputDir;
    private String backPressureDir;
    private String networkPrefix;
    private long sizeBackpressure;

    private final byte[] NEW_LINE_BYTES = "\n".getBytes();

    private boolean cbsDeliveryBlocked = false;

    private long numberOfMessages = 0;

    private final String HEADER = "<collection>";
    private final String FOOTER = "</collection>";


    private long processedRecords = 0;


    CBSFileWriter(ConfigHandler config) throws IOException {
        this.config = config;
        this.initialize();

    }



    boolean processBuffer(Collection<ConsumerRecord<String, String>> recordsBuffer){

        if (Boolean.parseBoolean(this.config.getUseZippedFiles())) {
            return writeZippedFile(recordsBuffer);
        } else {
            return writeFile(recordsBuffer);
        }

    }



    private void initialize() throws IOException {

        this.outputDir = config.getOutputDir().endsWith("/") ?
                config.getOutputDir() :  config.getOutputDir().concat("/");
        this.archiveDir = config.getArchiveDir().endsWith("/") ?
                config.getArchiveDir() :  config.getArchiveDir().concat("/");
        this.backPressureDir = config.getBackPressureDir().endsWith("/") ?
                config.getBackPressureDir() :  config.getBackPressureDir().concat("/");

        this.networkPrefix = config.getNetworkPrefix();

        this.sizeBackpressure = Long.parseLong(config.getSizeBackPressureCBS());

        this.cbsDeliveryBlocked = Boolean.parseBoolean(config.getDeliveryBlocked());

        if (! new File(this.outputDir).exists())
            Runtime.getRuntime().exec("mkdir -p " + this.outputDir);
        if (! new File(this.archiveDir).exists())
            Runtime.getRuntime().exec("mkdir -p " + this.archiveDir);
        if (! new File(this.backPressureDir).exists())
            Runtime.getRuntime().exec("mkdir -p " + this.backPressureDir);

        DateTimeFormatter outfileFormat = ofPattern("yyyy-MM-dd:kkmmss");
        fileWriterLogger.info(String.format("File-writer-process started at: %s",
                LocalDateTime.now().format(outfileFormat)));
    }

    private File openOutfile() {

        if (this.cbsDeliveryBlocked) {
            return  new File(this.backPressureDir.concat(this.defineFileName()));
        } else {
            return new File(this.archiveDir.concat(this.defineFileName()));
        }

    }


    private String defineFileName() {

        //we need a file name with format
        //networkPrefix-year Month Day Hour(24Hours) Minutes Seconds Milliseconds
        //because Kafka is so fast...
        //additionally a random part (beside milliseconds) for unique filenames
        DateTimeFormatter outfileFormat = ofPattern("yyyyMMddkkmmssSSS");

        String prefix = this.networkPrefix.concat("-").concat(LocalDateTime.now().format(outfileFormat)).
                concat("_").concat(generateRandomInt());

        if (Boolean.parseBoolean(this.config.getUseZippedFiles()))
            return prefix.concat(".xml.gz");
        else
            return prefix.concat(".xml");


    }


    private void createLogicalFileLink(String filename) throws IOException {

        String archiveFile = this.archiveDir.concat(filename);
        String resultsDirFile = this.outputDir.concat(filename);
        Runtime.getRuntime().exec("ln -s ".concat(archiveFile).concat(" ").concat(resultsDirFile));
    }

    long getNumberOfMessages() {
        return this.numberOfMessages;

    }


    private boolean writeZippedFile(Collection<ConsumerRecord<String, String>> recordsBuffer) {

        boolean messagesWritten = false;

        File f = openOutfile();
        //System.out.println(f.getAbsolutePath());

        try (FileOutputStream fos = new FileOutputStream(f))
        {

            GZIPOutputStream writer = new GZIPOutputStream(fos);

            writer.write(this.HEADER.getBytes(),0,this.HEADER.getBytes().length);
            writer.write(NEW_LINE_BYTES,0, NEW_LINE_BYTES.length);

            for (ConsumerRecord<String, String> record: recordsBuffer) {

                byte[] temp = record.value().getBytes();
                writer.write(temp,0,temp.length);
                writer.write(NEW_LINE_BYTES,0, NEW_LINE_BYTES.length);
                this.numberOfMessages++;
            }

            writer.write(this.FOOTER.getBytes(),0,this.FOOTER.getBytes().length);
            writer.write(NEW_LINE_BYTES,0, NEW_LINE_BYTES.length);

            writer.flush();
            writer.close();
            fos.close();

            if (!this.cbsDeliveryBlocked)
                this.createLogicalFileLink(f.getName());

            checkBlocking();

            messagesWritten = true;

        } catch (IOException | FileWriterException ex) {
            //log something
            ex.printStackTrace();
        }

        return messagesWritten;

    }

    private boolean writeFile(Collection<ConsumerRecord<String, String>> recordsBuffer) {

        boolean messagesWritten = false;

        File f = openOutfile();

        try (FileOutputStream fos = new FileOutputStream(f))
        {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(fos, StandardCharsets.UTF_8));

            writer.write(this.HEADER);
            writer.newLine();
            for (ConsumerRecord<String, String> record: recordsBuffer) {

                writer.write(record.value());
                writer.newLine();
                this.numberOfMessages++;
            }

            writer.write(this.FOOTER);
            writer.newLine();

            writer.flush();
            writer.close();
            fos.close();

            if (!this.cbsDeliveryBlocked)
                this.createLogicalFileLink(f.getName());

            checkBlocking();

            messagesWritten = true;

        } catch (IOException | FileWriterException ex) {
            //log something
            ex.printStackTrace();
        }

        return messagesWritten;

    }

    private void checkBlocking () throws FileWriterException{
        if (!this.cbsDeliveryBlocked && this.numberOfMessages > this.sizeBackpressure) {
            fileWriterLogger.info("content delivery to CBS will be blocked ");

            this.cbsDeliveryBlocked = true;
            this.config.setDeliveryBlocked(true);
            if (! this.config.serializeSourceConfig()) {
                fileWriterLogger.error("specialised config couldn't be serialized ");
                throw new FileWriterException("config File couldn't be serialized after setting setDeliveryBlocked" +
                        " to true");
            }
        }
    }

    private String generateRandomInt() {
        int min = 0;
        int max = 1000;

        return String.valueOf( ThreadLocalRandom.current().nextInt(min, max));
    }

}
