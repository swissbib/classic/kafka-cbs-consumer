package org.swissbib.kafka.consumer.exceptions;

public class FileWriterException extends Exception {

    public FileWriterException(String message) {
        super(message);
    }

}
