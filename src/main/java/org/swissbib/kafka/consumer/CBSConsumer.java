package org.swissbib.kafka.consumer;

import org.apache.kafka.clients.consumer.*;
import org.swissbib.kafka.consumer.config.ConfigHandler;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static java.time.format.DateTimeFormatter.ofPattern;

public class CBSConsumer {


    private static final Logger cbsConsumerLogger = LogManager.getLogger(CBSConsumer.class);
    //todo: read more about it
    //Example logging with log4j2 and yaml configuration
    //https://blog.scalyr.com/2018/08/log4j2-configuration-detailed-guide/
    //https://www.journaldev.com/7128/log4j2-example-tutorial-configuration-levels-appenders

    public static void main(String[] args) {

        ConfigHandler cH = null;

        try {
            cH = new ConfigHandler(args);
        } catch (Exception ex) {
            cbsConsumerLogger.error(ex);
            ex.printStackTrace();
            System.exit(1);
        }

        CBSFileWriter fw = null;
        try {
            fw = new CBSFileWriter(cH);
        } catch (IOException ex) {
            cbsConsumerLogger.error(ex);
            ex.printStackTrace();
            System.exit(1);
        }

        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG , cH.getBootstrapServers());
        props.put(ConsumerConfig.GROUP_ID_CONFIG, cH.getGroupid());
        //props.put("group.id", UUID.randomUUID().toString());
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, cH.getKeydeserializer());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, cH.getValuedeserializer());
        props.put(ConsumerConfig.CLIENT_ID_CONFIG, cH.getKafkaClientID());
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, Boolean.valueOf(cH.getEnableAutoCommitConfig()));
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, cH.getAutoOffsetResetConfig());

        DateTimeFormatter outfileFormat = ofPattern("yyyy-MM-dd:kkmmss");
        cbsConsumerLogger.info(String.format("consumer process for group %s started at: %s",
                cH.getGroupid(),
                LocalDateTime.now().format(outfileFormat)));


        ArrayList<ConsumerRecord<String,String>> bufferList = new ArrayList<>();

        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);

        consumer.subscribe(Collections.singletonList(cH.getKafkaTopic()));

        long maxBufferSize = Long.parseLong(cH.getBufferFileSize());

        int countRecordlessPolling = 0;

        try {
            while (countRecordlessPolling <= 20) {

                //todo: make this configurable
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(3000));
                //ConsumerRecords<String, String> records = consumer.poll(1000);

                if (records.count() == 0) {
                    countRecordlessPolling++;
                }

                for (ConsumerRecord<String, String> record : records) {

                    bufferList.add(record);
                }

                if (bufferList.size() >= maxBufferSize) {
                    if (fw.processBuffer(bufferList)) {
                        bufferList.clear();
                        consumer.commitSync();
                    } else {
                        throw new Exception();
                    }
                }
            }

            if (bufferList.size() > 0) {
                if (fw.processBuffer(bufferList)) {
                    bufferList.clear();
                    consumer.commitSync();
                }
            }
        } catch (Exception ex) {
            cbsConsumerLogger.error("exception was thrown",ex);
            ex.printStackTrace();
        } finally {

            cbsConsumerLogger.info(String.format("consumer process for group %s finished at: %s",
                    cH.getGroupid(),
                    LocalDateTime.now().format(outfileFormat)));

            cbsConsumerLogger.info("summary");
            cbsConsumerLogger.info(String.format("number of written messages %s: from topic: %s\n\n\n",
                    fw.getNumberOfMessages(),cH.getKafkaTopic()));
            consumer.close();

        }

    }

}
