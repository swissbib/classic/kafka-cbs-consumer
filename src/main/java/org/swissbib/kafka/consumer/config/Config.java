package org.swissbib.kafka.consumer.config;

import java.util.Map;
import java.util.Optional;

public abstract class Config {

    public Map<String, String> kafka;

    public  Map<String, String> cbs;

    Optional<String> getBootstrapServer() {
        return  this.kafka.containsKey("bootstrap.servers") ? Optional.of(this.kafka.get("bootstrap.servers")) :
                Optional.empty();
    }

    Optional<String> getKafkaTopic() {
        return  this.kafka.containsKey("topic") ? Optional.of(this.kafka.get("topic")) :
                Optional.empty();
    }

    Optional<String> getOutputDir() {
        return  this.cbs.containsKey("outputdir") ? Optional.of(this.cbs.get("outputdir")) :
                Optional.empty();
    }

    Optional<String> getArchiveDir() {
        return  this.cbs.containsKey("archiveDir") ? Optional.of(this.cbs.get("archiveDir")) :
                Optional.empty();
    }

    Optional<String> getNetworkPrefix() {
        return  this.cbs.containsKey("networkprefix") ? Optional.of(this.cbs.get("networkprefix")) :
                Optional.empty();
    }

    Optional<String> getAutoOffsetResetConfig() {
        return  this.kafka.containsKey("auto_offset_reset_config") ? Optional.of(this.kafka.get("auto_offset_reset_config")) :
                Optional.empty();

    }

    Optional<String> getEnableAutoCommitConfig() {
        return  this.kafka.containsKey("enable_auto_commit_config") ? Optional.of(this.kafka.get("enable_auto_commit_config")) :
                Optional.empty();

    }



    Optional<String> getGroupid() {
        return  this.kafka.containsKey("group.id") ? Optional.of(this.kafka.get("group.id")) :
                Optional.empty();
    }

    Optional<String> getKeydeserializer() {
        return  this.kafka.containsKey("key.deserializer") ? Optional.of(this.kafka.get("key.deserializer")) :
                Optional.empty();
    }

    Optional<String> getValuedeserializer() {
        return  this.kafka.containsKey("value.deserializer") ? Optional.of(this.kafka.get("value.deserializer")) :
                Optional.empty();
    }

    Optional<String> getDeliveryBlocked() {
        return  this.cbs.containsKey("deliveryBlocked") ? Optional.of(this.cbs.get("deliveryBlocked")) :
                Optional.empty();
    }

    Optional<String> getKafkaClientID() {
        return  this.kafka.containsKey("clientid") ? Optional.of(this.kafka.get("clientid")) :
                Optional.empty();
    }

    Optional<String> getSizeBackPressureCBS() {
        return  this.cbs.containsKey("sizeBackPressureCBS") ? Optional.of(this.cbs.get("sizeBackPressureCBS")) :
                Optional.empty();
    }

    Optional<String> getBufferFileSize() {
        return  this.cbs.containsKey("bufferFileSize") ? Optional.of(this.cbs.get("bufferFileSize")) :
                Optional.empty();
    }

    Optional<String> getBackPressureDir() {
        return  this.cbs.containsKey("backPressureDir") ? Optional.of(this.cbs.get("backPressureDir")) :
                Optional.empty();
    }


    Optional<String> getUseZippedFiles() {
        return  this.cbs.containsKey("zippedFiles") ? Optional.of(this.cbs.get("zippedFiles")) :
                Optional.empty();
    }


    void setDeliveryBlocked(boolean blocked) {
        this.cbs.put("deliveryBlocked", String.valueOf(blocked));
    }



}
