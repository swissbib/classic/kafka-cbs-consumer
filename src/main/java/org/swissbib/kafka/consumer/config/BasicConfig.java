package org.swissbib.kafka.consumer.config;

import java.util.Map;

public class BasicConfig  extends Config {


    public void setKafka(Map<String, String> kafka) {
        this.kafka = kafka;
    }

    public void setCbs(Map<String, String> cbs) {
        this.cbs = cbs;
    }

    public Map<String, String> getKafka() {

        return this.kafka;
    }

    public Map<String, String> getCbs() {

        return this.cbs;
    }


}
