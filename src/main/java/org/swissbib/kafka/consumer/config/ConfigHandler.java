package org.swissbib.kafka.consumer.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.apache.commons.cli.*;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;


public class ConfigHandler {


    private BasicConfig basicConfig;
    private SourceConfig sourceConfig;
    private String sourceConfigFile;
    private String baseConfigFile;
    private String configPath;


    public ConfigHandler(String[] taskOptions) throws Exception {

        this.evaluateOptions(taskOptions);
        this.loadJsonConfig();

    }


    private void evaluateOptions(String[] taskOptions) throws ParseException {

        Options options = new Options();
        options.addOption("b", "baseConfig", true, "basic Configuration");
        options.addOption("s", "sourceConfig", true, "source Configuration specialised for network");
        options.addOption("c", "configPath", true, "path for configurations");

        CommandLineParser parser = new DefaultParser();

        CommandLine cmd = parser.parse(options, taskOptions);

        assert null != cmd;

        if  (!(cmd.hasOption("s") || cmd.hasOption("source"))) {
            this.printUsage();
            throw new ParseException("invalid task options");
        }

        this.sourceConfigFile = cmd.hasOption("s") ? cmd.getOptionValue("s").trim() : cmd.getOptionValue("s").trim();

        if (cmd.hasOption("b") || cmd.hasOption("baseConfig")) {
            this.baseConfigFile = cmd.hasOption("b") ? cmd.getOptionValue("b").trim() : cmd.getOptionValue("baseConfig").trim();
        } else {
            this.baseConfigFile = "cbsbasic.yaml";
        }

        if (cmd.hasOption("c") || cmd.hasOption("configPath")) {
            this.configPath = cmd.hasOption("c") ? cmd.getOptionValue("c").trim() : cmd.getOptionValue("configPath").trim();
            this.configPath = this.configPath.endsWith("/") ?
                    this.configPath :  this.configPath.concat("/");
        } else {
            this.configPath = "configs/";
        }





    }

    private void loadJsonConfig() {

        //URL basicConfigUrl = Thread.currentThread().getContextClassLoader().getResource(this.baseConfigFile);
        //URL sourceConfigUrl = Thread.currentThread().getContextClassLoader().getResource(this.sourceConfigFile);

        File basicConfigUrl = null;
        File sourceConfigUrl = null;


        basicConfigUrl = new File(this.configPath + this.baseConfigFile);
        sourceConfigUrl = new File(this.configPath + this.sourceConfigFile);


        //assert basicConfigUrl != null;
        try (InputStream is = new  FileInputStream(basicConfigUrl)) {

            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            this.basicConfig = mapper.readValue(is,BasicConfig.class);

        } catch (IOException ioException) {
            throw new Error(String.format("error openening the basicConfig %s", basicConfig));
        }

        //assert sourceConfigUrl != null;
        try (InputStream is = new FileInputStream(sourceConfigUrl)) {

            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            this.sourceConfig = mapper.readValue(is,SourceConfig.class);

        } catch (IOException ioException) {
            throw new Error(String.format("error openening the sourceConfig %s", sourceConfig));
        }

    }



    private void printUsage() {
        System.out.println("wrong options");
        System.out.println("usage: consumer -s sourceConfigFileName [-b baseConfigFileName]");
        System.out.println("usage: consumer -sourceConfig sourceConfigFileName [-baseConfig baseConfigFileName]");
    }

    public String getBootstrapServers() {

        return  this.sourceConfig.getBootstrapServer().orElseGet(() -> this.basicConfig.getBootstrapServer().get()) ;

    }

    public String getKafkaTopic() {

        return  this.sourceConfig.getKafkaTopic().orElseGet(() -> this.basicConfig.getBootstrapServer().get());


    }

    public String getOutputDir() {

        return  this.sourceConfig.getOutputDir().orElseGet(() ->this.basicConfig.getOutputDir().get());

    }


    public String getArchiveDir() {

        return  this.sourceConfig.getArchiveDir().orElseGet(() ->this.basicConfig.getArchiveDir().get());

    }

    public String getNetworkPrefix() {

        return  this.sourceConfig.getNetworkPrefix().orElseGet(() ->this.basicConfig.getNetworkPrefix().get());

    }


    public String getSizeBackPressureCBS() {

        return this.sourceConfig.getSizeBackPressureCBS().orElseGet(() ->
            (this.basicConfig.getSizeBackPressureCBS().isPresent() && !this.basicConfig.getSizeBackPressureCBS().get().isEmpty()
                    && StringUtils.isNumeric(this.basicConfig.getSizeBackPressureCBS().get())) ?
                    this.basicConfig.getSizeBackPressureCBS().get() :
                    "200000"
        );
    }

    public String getBufferFileSize() {

        return this.sourceConfig.getBufferFileSize().orElseGet(() ->
                (this.basicConfig.getBufferFileSize().isPresent() && !this.basicConfig.getBufferFileSize().get().isEmpty()
                        && StringUtils.isNumeric(this.basicConfig.getBufferFileSize().get())) ?
                        this.basicConfig.getBufferFileSize().get() :
                        "200000"
        );
    }



    public String getGroupid() {

        return  this.sourceConfig.getGroupid().orElseGet(() -> this.basicConfig.getGroupid().get());

    }

    public String getKeydeserializer() {

        return  this.sourceConfig.getKeydeserializer().orElseGet(() -> this.basicConfig.getKeydeserializer().get());

    }

    public String getValuedeserializer() {

        return  this.sourceConfig.getValuedeserializer().orElseGet(() -> this.basicConfig.getValuedeserializer().get());

    }

    public String getEnableAutoCommitConfig() {

        return  this.sourceConfig.getEnableAutoCommitConfig().orElseGet(() -> this.basicConfig.getEnableAutoCommitConfig().get());
    }

    public String getAutoOffsetResetConfig() {
        return  this.sourceConfig.getAutoOffsetResetConfig().orElseGet(() -> this.basicConfig.getAutoOffsetResetConfig().get());
    }

    public String getDeliveryBlocked() {

        return  this.sourceConfig.getDeliveryBlocked().orElseGet(() -> this.basicConfig.getDeliveryBlocked().get());
    }


    public void setDeliveryBlocked(boolean blocked) {

        //values set programmatically should only be set in the source specific configuration
        this.sourceConfig.setDeliveryBlocked(blocked);
    }


    public String getBackPressureDir() {
        return  this.sourceConfig.getBackPressureDir().orElseGet(() -> this.basicConfig.getBackPressureDir().get());
    }


    public String getKafkaClientID() {

        return  this.sourceConfig.getKafkaClientID().orElseGet(() -> this.basicConfig.getKafkaClientID().get());

    }


    public String getUseZippedFiles() {
        return  this.sourceConfig.getUseZippedFiles().orElseGet(() -> this.basicConfig.getUseZippedFiles().get());
    }



    public boolean serializeSourceConfig() {

        boolean writeSuccessfull = true;

        try {
            YAMLFactory yf = new YAMLFactory();
            ObjectMapper mapper = new ObjectMapper(yf);
            //String test = mapper.writeValueAsString(this.sourceConfig);
            mapper.writeValue(new FileWriter(new File(this.configPath + this.sourceConfigFile)),this.sourceConfig);

        } catch ( IOException jpe) {
            //todo
            writeSuccessfull = false;
            jpe.printStackTrace();
        }

        return writeSuccessfull;
    }





}
