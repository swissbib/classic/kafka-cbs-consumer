package org.swissbib.kafka.consumer.config;

import java.util.Map;

public class SourceConfig extends Config {


    public void setKafka(Map<String, String> kafka) {
        this.kafka = kafka;
    }

    public void setCbs(Map<String, String> cbs) {
        this.cbs = cbs;
    }
}
