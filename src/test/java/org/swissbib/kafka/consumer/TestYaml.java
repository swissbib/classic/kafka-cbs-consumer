package org.swissbib.kafka.consumer;

import java.util.Map;

public class TestYaml {


    private Map<String, Map<String,String>> adminclient;


    public Map<String,Map<String,String>> getAdminclient() {
        return adminclient;
    }

    public void setAdminclient(Map<String,Map<String,String>> adminclient) {
        this.adminclient = adminclient;
    }


}
