package org.swissbib.kafka.consumer;

import org.apache.commons.cli.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.engine.support.hierarchical.SingleTestExecutor;
import org.swissbib.kafka.consumer.config.ConfigHandler;

import java.util.Optional;

public class TestArgumentParser {

    private String[] argsShort;
    private String[] argsLong;
    private String[] argsShortOptional;

    private String[] sourceOnlyShort;
    private String[] sourceMissingArgsThrowExcep;

    private String[] sourceOnlyLong;

    private String[] sourceConfigFileNotAvailable;

    private String[] optionsOnlyBaseConfigNoSource;


    @BeforeEach
    void setup() {
        argsShort = new String[] {"-b baseConfig", "-s sourceConfig"};
        argsLong = new String[] {"-baseConfig baseConfigLong", "-sourceConfig sourceConfigLong"};
        argsShortOptional = new String[] {"-s sourceConfig"};

        sourceOnlyShort = new String[] {"-s cbsidssg.yaml"};
        sourceMissingArgsThrowExcep = new String[] {};

        sourceOnlyLong = new String[] {"-sourceConfig cbsidssg.yaml"};

        sourceConfigFileNotAvailable = new String[] {"-sourceConfig blabla.yaml"};

        optionsOnlyBaseConfigNoSource = new String[] {"-b cbsbasic.yaml"};

    }


    @Test
    void testArgumentsParsing() throws ParseException {

        Options options = new Options();

        options.addOption("b", "baseConfig" ,true, "basic Configuration");
        options.addOption("s", "sourceConfig" ,true, "source Configuration specialised for network");

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse( options, argsShort);


        Assertions.assertTrue(cmd.getOptionValue("b").trim().equalsIgnoreCase("baseConfig"));
        Assertions.assertTrue(cmd.getOptionValue("s").trim().equalsIgnoreCase("sourceConfig"));

        cmd = parser.parse(options,argsLong);
        Assertions.assertTrue(cmd.getOptionValue("baseConfig").trim().equalsIgnoreCase("baseConfigLong"));
        Assertions.assertTrue(cmd.getOptionValue("sourceConfig").trim().equalsIgnoreCase("sourceConfigLong"));

        cmd = parser.parse(options,argsShortOptional);
        Assertions.assertFalse(cmd.hasOption("b"));

    }

    @Test
    void testConfigHandler() throws Exception{
        ConfigHandler cH  = new ConfigHandler(sourceOnlyShort);

        Assertions.assertNotNull(cH);

        Assertions.assertThrows(ParseException.class, () -> new ConfigHandler(sourceMissingArgsThrowExcep),
                "das war nichts, Args ohne Options...");

        cH = new ConfigHandler(sourceOnlyLong);

        Assertions.assertNotNull(cH);

        Assertions.assertTrue(cH.getBootstrapServers().equalsIgnoreCase("hsg.ch:9092"));
        Assertions.assertTrue(cH.getKafkaTopic().equalsIgnoreCase("idssgsPezial"));
        Assertions.assertTrue(cH.getMaxDocs().equalsIgnoreCase("99999"));
        Assertions.assertTrue(cH.getOutputDir().equalsIgnoreCase("/outdir/nur/fuer/idssg"));
        Assertions.assertTrue(! cH.getGroupid().isEmpty());
        Assertions.assertTrue(! cH.getKeydeserializer().isEmpty());
        Assertions.assertTrue(! cH.getValuedeserializer().isEmpty());



        Assertions.assertThrows(AssertionError.class, () -> new ConfigHandler(sourceConfigFileNotAvailable),
                "das war nichts, source Config File not available...");

        Assertions.assertThrows(ParseException.class, () -> new ConfigHandler(optionsOnlyBaseConfigNoSource),
                "das war nichts, no argument option for sourceConfig...");



    }

}
