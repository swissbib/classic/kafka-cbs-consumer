package org.swissbib.kafka.consumer;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;


import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Map;

class TestJacksonHandling {

    //Beispiele Lesen von Yaml Files mit Jackson
    //https://dzone.com/articles/read-yaml-in-java-with-jackson
    //https://learning.oreilly.com/library/view/json-at-work/9781491982389/ch04.html


    @Test
    void testReadJson() throws IOException {

        URL url = Thread.currentThread().getContextClassLoader().getResource("cbstest.yaml");

        Assertions.assertNotNull(url);
        InputStream is = url.openStream();
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        TestYaml ty = mapper.readValue(is,TestYaml.class);
        Map<String, Map<String, String>> adminClient = ty.getAdminclient();
        Assertions.assertTrue(adminClient.containsKey("conf"));


    }

    @Test
    void testSimpleKeyvalues() throws IOException {
        URL url = Thread.currentThread().getContextClassLoader().getResource("simpleKeyValues.yaml");
        Assertions.assertNotNull(url);
        InputStream is = url.openStream();
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        Map<String, String> sV  = mapper.readValue(is, new TypeReference<Map<String,String>>(){});
        Assertions.assertTrue(sV.containsKey("test1"));
    }


    @Test
    void testJacksonTreeType() throws IOException {
        URL url = Thread.currentThread().getContextClassLoader().getResource("simpleKeyValues.yaml");
        Assertions.assertNotNull(url);
        InputStream is = url.openStream();
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        JsonNode node  = mapper.readTree(is);
        Assertions.assertTrue(node.get("test4").textValue().equalsIgnoreCase("dies ist value von test 4"));
        Assertions.assertNull(node.get("test5"));
    }

    @Test
    void testJacksonComplexTreeType() throws IOException {
        URL url = Thread.currentThread().getContextClassLoader().getResource("cbstest.yaml");
        Assertions.assertNotNull(url);
        InputStream is = url.openStream();
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        JsonNode node  = mapper.readTree(is);
        //System.out.println(ReflectionToStringBuilder.toString(node));
        JsonNode confNode = node.get("adminclient").get("conf");
        Assertions.assertNotNull(confNode);


    }



}
