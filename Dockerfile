FROM openjdk:8
ADD . /
WORKDIR /
RUN ./gradlew -q --no-scan --no-daemon --no-build-cache shadowJar

FROM openjdk:8-jre-alpine
RUN mkdir /app




COPY --from=0 /build/libs/consumercbs-0.2-all.jar /app
WORKDIR /app

USER 55864:55864

VOLUME /configs
VOLUME /archive
VOLUME /backpressure
VOLUME /results
VOLUME /logging

CMD java -jar consumercbs-0.2-all.jar


#docker container run --rm  --network host -v $(pwd)/configs:/configs -v $(pwd)/results:/results -v $(pwd)/archive:/archive -v $(pwd)/backpressure:/backpressure cbsconsumer java -jar /app/consumercbs-0.2-all.jar -s cbsidsbb.yaml
#docker container run --rm  -v $(pwd)/configs:/configs -v $(pwd)/results:/results -v $(pwd)/archive:/archive -v $(pwd)/backpressure:/backpressure cbsconsumer java -jar /app/consumercbs-0.2-all.jar -s cbsidsbb.yaml


# docker container run --rm -v $(pwd)/logging:/logging -v $(pwd)/configs:/configs -v $(pwd)/results:/results -v $(pwd)/archive:/archive -v $(pwd)/backpressure:/backpressure cbsconsumer java -Dlog4j.configurationFile=/logging/cbsconsumer.yaml -jar /app/consumercbs-0.2-all.jar -s cbsidsbb.yaml

